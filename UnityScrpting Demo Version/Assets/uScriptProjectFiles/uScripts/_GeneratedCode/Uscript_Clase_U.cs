//uScript Generated Code - Build 1.1.3130
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class Uscript_Clase_U : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.Vector3 local_10_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_11_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_12_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_2_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_4_System_Single = (float) 0;
   System.Single local_5_System_Single = (float) 0;
   System.Single local_8_System_Single = (float) 0;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_15 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_ClampVector3 logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_1 = new uScriptAct_ClampVector3( );
   UnityEngine.Vector3 logic_uScriptAct_ClampVector3_Target_1 = new Vector3( );
   System.Boolean logic_uScriptAct_ClampVector3_ClampX_1 = (bool) false;
   System.Single logic_uScriptAct_ClampVector3_XMin_1 = (float) 0;
   System.Single logic_uScriptAct_ClampVector3_XMax_1 = (float) 0;
   System.Boolean logic_uScriptAct_ClampVector3_ClampY_1 = (bool) false;
   System.Single logic_uScriptAct_ClampVector3_YMin_1 = (float) 0;
   System.Single logic_uScriptAct_ClampVector3_YMax_1 = (float) 0;
   System.Boolean logic_uScriptAct_ClampVector3_ClampZ_1 = (bool) false;
   System.Single logic_uScriptAct_ClampVector3_ZMin_1 = (float) 0;
   System.Single logic_uScriptAct_ClampVector3_ZMax_1 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_ClampVector3_Result_1;
   bool logic_uScriptAct_ClampVector3_Out_1 = true;
   //pointer to script instanced logic node
   uScriptAct_GetDeltaTime logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_3 = new uScriptAct_GetDeltaTime( );
   System.Single logic_uScriptAct_GetDeltaTime_DeltaTime_3;
   System.Single logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_3;
   System.Single logic_uScriptAct_GetDeltaTime_FixedDeltaTime_3;
   bool logic_uScriptAct_GetDeltaTime_Out_3 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_6 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_6 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_6 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_6;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_6;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_6 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_7 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_7 = new Vector3( );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_7 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_7;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_7 = true;
   //pointer to script instanced logic node
   uScriptAct_GetScaleFromTransform logic_uScriptAct_GetScaleFromTransform_uScriptAct_GetScaleFromTransform_9 = new uScriptAct_GetScaleFromTransform( );
   UnityEngine.Transform logic_uScriptAct_GetScaleFromTransform_target_9 = default(UnityEngine.Transform);
   System.Boolean logic_uScriptAct_GetScaleFromTransform_getLossy_9 = (bool) false;
   UnityEngine.Vector3 logic_uScriptAct_GetScaleFromTransform_scale_9;
   System.Single logic_uScriptAct_GetScaleFromTransform_xScale_9;
   System.Single logic_uScriptAct_GetScaleFromTransform_yScale_9;
   System.Single logic_uScriptAct_GetScaleFromTransform_zScale_9;
   bool logic_uScriptAct_GetScaleFromTransform_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_AddVector3_v2 logic_uScriptAct_AddVector3_v2_uScriptAct_AddVector3_v2_13 = new uScriptAct_AddVector3_v2( );
   UnityEngine.Vector3 logic_uScriptAct_AddVector3_v2_A_13 = new Vector3( );
   UnityEngine.Vector3 logic_uScriptAct_AddVector3_v2_B_13 = new Vector3( );
   UnityEngine.Vector3 logic_uScriptAct_AddVector3_v2_Result_13;
   bool logic_uScriptAct_AddVector3_v2_Out_13 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectScale logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_14 = new uScriptAct_SetGameObjectScale( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectScale_Target_14 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectScale_Scale_14 = new Vector3( );
   bool logic_uScriptAct_SetGameObjectScale_Out_14 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == owner_Connection_15 || false == m_RegisteredForEvents )
      {
         owner_Connection_15 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_0;
                  component.OnLateUpdate += Instance_OnLateUpdate_0;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_0;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_0;
               component.OnLateUpdate -= Instance_OnLateUpdate_0;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_1.SetParent(g);
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_3.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_6.SetParent(g);
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_7.SetParent(g);
      logic_uScriptAct_GetScaleFromTransform_uScriptAct_GetScaleFromTransform_9.SetParent(g);
      logic_uScriptAct_AddVector3_v2_uScriptAct_AddVector3_v2_13.SetParent(g);
      logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_14.SetParent(g);
      owner_Connection_15 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnUpdate_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_0( );
   }
   
   void Instance_OnLateUpdate_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_0( );
   }
   
   void Instance_OnFixedUpdate_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_0( );
   }
   
   void Relay_OnUpdate_0()
   {
      Relay_In_1();
   }
   
   void Relay_OnLateUpdate_0()
   {
   }
   
   void Relay_OnFixedUpdate_0()
   {
   }
   
   void Relay_In_1()
   {
      {
         {
            logic_uScriptAct_ClampVector3_Target_1 = local_2_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_1.In(logic_uScriptAct_ClampVector3_Target_1, logic_uScriptAct_ClampVector3_ClampX_1, logic_uScriptAct_ClampVector3_XMin_1, logic_uScriptAct_ClampVector3_XMax_1, logic_uScriptAct_ClampVector3_ClampY_1, logic_uScriptAct_ClampVector3_YMin_1, logic_uScriptAct_ClampVector3_YMax_1, logic_uScriptAct_ClampVector3_ClampZ_1, logic_uScriptAct_ClampVector3_ZMin_1, logic_uScriptAct_ClampVector3_ZMax_1, out logic_uScriptAct_ClampVector3_Result_1);
      local_2_UnityEngine_Vector3 = logic_uScriptAct_ClampVector3_Result_1;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_1.Out;
      
      if ( test_0 == true )
      {
         Relay_In_3();
      }
   }
   
   void Relay_In_3()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_3.In(out logic_uScriptAct_GetDeltaTime_DeltaTime_3, out logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_3, out logic_uScriptAct_GetDeltaTime_FixedDeltaTime_3);
      local_4_System_Single = logic_uScriptAct_GetDeltaTime_DeltaTime_3;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_3.Out;
      
      if ( test_0 == true )
      {
         Relay_In_6();
      }
   }
   
   void Relay_In_6()
   {
      {
         {
            logic_uScriptAct_MultiplyFloat_v2_A_6 = local_5_System_Single;
            
         }
         {
            logic_uScriptAct_MultiplyFloat_v2_B_6 = local_4_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_6.In(logic_uScriptAct_MultiplyFloat_v2_A_6, logic_uScriptAct_MultiplyFloat_v2_B_6, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_6, out logic_uScriptAct_MultiplyFloat_v2_IntResult_6);
      local_8_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_6;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_6.Out;
      
      if ( test_0 == true )
      {
         Relay_In_7();
      }
   }
   
   void Relay_In_7()
   {
      {
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_7 = local_2_UnityEngine_Vector3;
            
         }
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_7 = local_8_System_Single;
            
         }
         {
         }
      }
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_7.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_7, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_7, out logic_uScriptAct_MultiplyVector3WithFloat_Result_7);
      local_10_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_7;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_7.Out;
      
      if ( test_0 == true )
      {
         Relay_In_9();
      }
   }
   
   void Relay_In_9()
   {
      {
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetScaleFromTransform_uScriptAct_GetScaleFromTransform_9.In(logic_uScriptAct_GetScaleFromTransform_target_9, logic_uScriptAct_GetScaleFromTransform_getLossy_9, out logic_uScriptAct_GetScaleFromTransform_scale_9, out logic_uScriptAct_GetScaleFromTransform_xScale_9, out logic_uScriptAct_GetScaleFromTransform_yScale_9, out logic_uScriptAct_GetScaleFromTransform_zScale_9);
      local_11_UnityEngine_Vector3 = logic_uScriptAct_GetScaleFromTransform_scale_9;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetScaleFromTransform_uScriptAct_GetScaleFromTransform_9.Out;
      
      if ( test_0 == true )
      {
         Relay_In_13();
      }
   }
   
   void Relay_In_13()
   {
      {
         {
            logic_uScriptAct_AddVector3_v2_A_13 = local_10_UnityEngine_Vector3;
            
         }
         {
            logic_uScriptAct_AddVector3_v2_B_13 = local_11_UnityEngine_Vector3;
            
         }
         {
         }
      }
      logic_uScriptAct_AddVector3_v2_uScriptAct_AddVector3_v2_13.In(logic_uScriptAct_AddVector3_v2_A_13, logic_uScriptAct_AddVector3_v2_B_13, out logic_uScriptAct_AddVector3_v2_Result_13);
      local_12_UnityEngine_Vector3 = logic_uScriptAct_AddVector3_v2_Result_13;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AddVector3_v2_uScriptAct_AddVector3_v2_13.Out;
      
      if ( test_0 == true )
      {
         Relay_In_14();
      }
   }
   
   void Relay_In_14()
   {
      {
         {
            int index = 0;
            if ( logic_uScriptAct_SetGameObjectScale_Target_14.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectScale_Target_14, index + 1);
            }
            logic_uScriptAct_SetGameObjectScale_Target_14[ index++ ] = owner_Connection_15;
            
         }
         {
            logic_uScriptAct_SetGameObjectScale_Scale_14 = local_12_UnityEngine_Vector3;
            
         }
      }
      logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_14.In(logic_uScriptAct_SetGameObjectScale_Target_14, logic_uScriptAct_SetGameObjectScale_Scale_14);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
}
