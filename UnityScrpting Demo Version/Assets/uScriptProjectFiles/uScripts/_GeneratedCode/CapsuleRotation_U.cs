//uScript Generated Code - Build 1.1.3130
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class CapsuleRotation_U : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   System.Single local_10_System_Single = (float) 0;
   UnityEngine.Vector3 local_12_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_7_System_Single = (float) 0;
   UnityEngine.Vector3 local_Ejes_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_Speed_System_Single = (float) 1;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_4 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_ClampVector3 logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_1 = new uScriptAct_ClampVector3( );
   UnityEngine.Vector3 logic_uScriptAct_ClampVector3_Target_1 = new Vector3( );
   System.Boolean logic_uScriptAct_ClampVector3_ClampX_1 = (bool) true;
   System.Single logic_uScriptAct_ClampVector3_XMin_1 = (float) -1;
   System.Single logic_uScriptAct_ClampVector3_XMax_1 = (float) 1;
   System.Boolean logic_uScriptAct_ClampVector3_ClampY_1 = (bool) true;
   System.Single logic_uScriptAct_ClampVector3_YMin_1 = (float) -1;
   System.Single logic_uScriptAct_ClampVector3_YMax_1 = (float) 1;
   System.Boolean logic_uScriptAct_ClampVector3_ClampZ_1 = (bool) true;
   System.Single logic_uScriptAct_ClampVector3_ZMin_1 = (float) -1;
   System.Single logic_uScriptAct_ClampVector3_ZMax_1 = (float) 1;
   UnityEngine.Vector3 logic_uScriptAct_ClampVector3_Result_1;
   bool logic_uScriptAct_ClampVector3_Out_1 = true;
   //pointer to script instanced logic node
   uScriptAct_GetDeltaTime logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_6 = new uScriptAct_GetDeltaTime( );
   System.Single logic_uScriptAct_GetDeltaTime_DeltaTime_6;
   System.Single logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_6;
   System.Single logic_uScriptAct_GetDeltaTime_FixedDeltaTime_6;
   bool logic_uScriptAct_GetDeltaTime_Out_6 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_8 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_8 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_8 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_8;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_8;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_8 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_9 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_9 = new Vector3( );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_9 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_9;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_9 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   UnityEngine.Vector3 method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_eulers_11 = new Vector3( );
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == owner_Connection_4 || false == m_RegisteredForEvents )
      {
         owner_Connection_4 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_0;
                  component.OnLateUpdate += Instance_OnLateUpdate_0;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_0;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_0;
               component.OnLateUpdate -= Instance_OnLateUpdate_0;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_1.SetParent(g);
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_6.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_8.SetParent(g);
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_9.SetParent(g);
      owner_Connection_4 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnUpdate_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_0( );
   }
   
   void Instance_OnLateUpdate_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_0( );
   }
   
   void Instance_OnFixedUpdate_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_0( );
   }
   
   void Relay_OnUpdate_0()
   {
      Relay_In_1();
   }
   
   void Relay_OnLateUpdate_0()
   {
   }
   
   void Relay_OnFixedUpdate_0()
   {
   }
   
   void Relay_In_1()
   {
      {
         {
            logic_uScriptAct_ClampVector3_Target_1 = local_Ejes_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_1.In(logic_uScriptAct_ClampVector3_Target_1, logic_uScriptAct_ClampVector3_ClampX_1, logic_uScriptAct_ClampVector3_XMin_1, logic_uScriptAct_ClampVector3_XMax_1, logic_uScriptAct_ClampVector3_ClampY_1, logic_uScriptAct_ClampVector3_YMin_1, logic_uScriptAct_ClampVector3_YMax_1, logic_uScriptAct_ClampVector3_ClampZ_1, logic_uScriptAct_ClampVector3_ZMin_1, logic_uScriptAct_ClampVector3_ZMax_1, out logic_uScriptAct_ClampVector3_Result_1);
      local_Ejes_UnityEngine_Vector3 = logic_uScriptAct_ClampVector3_Result_1;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_1.Out;
      
      if ( test_0 == true )
      {
         Relay_In_6();
      }
   }
   
   void Relay_In_6()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_6.In(out logic_uScriptAct_GetDeltaTime_DeltaTime_6, out logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_6, out logic_uScriptAct_GetDeltaTime_FixedDeltaTime_6);
      local_7_System_Single = logic_uScriptAct_GetDeltaTime_DeltaTime_6;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_6.Out;
      
      if ( test_0 == true )
      {
         Relay_In_8();
      }
   }
   
   void Relay_In_8()
   {
      {
         {
            logic_uScriptAct_MultiplyFloat_v2_A_8 = local_Speed_System_Single;
            
         }
         {
            logic_uScriptAct_MultiplyFloat_v2_B_8 = local_7_System_Single;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_8.In(logic_uScriptAct_MultiplyFloat_v2_A_8, logic_uScriptAct_MultiplyFloat_v2_B_8, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_8, out logic_uScriptAct_MultiplyFloat_v2_IntResult_8);
      local_10_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_8;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_8.Out;
      
      if ( test_0 == true )
      {
         Relay_In_9();
      }
   }
   
   void Relay_In_9()
   {
      {
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_9 = local_Ejes_UnityEngine_Vector3;
            
         }
         {
            logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_9 = local_10_System_Single;
            
         }
         {
         }
      }
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_9.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_9, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_9, out logic_uScriptAct_MultiplyVector3WithFloat_Result_9);
      local_12_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_9;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_9.Out;
      
      if ( test_0 == true )
      {
         Relay_Rotate_11();
      }
   }
   
   void Relay_Rotate_11()
   {
      {
         {
         }
         {
            method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_eulers_11 = local_12_UnityEngine_Vector3;
            
         }
      }
      {
         UnityEngine.Transform component;
         component = owner_Connection_4.GetComponent<UnityEngine.Transform>();
         if ( null != component )
         {
            component.Rotate(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_eulers_11);
         }
      }
   }
   
}
